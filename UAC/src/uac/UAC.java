package uac;

public class UAC {

    private PI inicio;
    private PI fin;
    
    public UAC(){
        inicio=fin=null;
    }
    
    public boolean estaVacia(){
        return inicio == null;
    }
    
    
        public void agregarInicio(int elemento){
            if (!estaVacia()){
                inicio= new PI(elemento, inicio, null);
                inicio.siguiente.anterior = inicio;
            }else{
                inicio=fin=new PI(elemento);
            }
        }
    
        public void agregarFinal(int elemento){
            if (!estaVacia()){
                fin= new PI(elemento, null, fin);
                fin.anterior.siguiente = fin;
            }else{
                inicio=fin=new PI(elemento);
            }
        }


        public void ListaInicioaFin(){
            if (!estaVacia()){
                String conector = "<=>";
                String datos = "<-";
                PI auxiliar = inicio;
                        while(auxiliar !=null){
                           datos = datos +"["+auxiliar.dato+"] "+conector;
                           auxiliar = auxiliar.siguiente;
                        }
                datos += "->"; 
                System.out.println(datos);
            }
        }

        public void ListaFinaInicio(){
            if (!estaVacia()){
                String conector= "<=>";
                String datos = " ";
                PI auxiliar = fin;
                    while(auxiliar !=null){
                       datos = datos +"["+auxiliar.dato+"]"+conector;
                       auxiliar = auxiliar.anterior;
                    }
                System.out.println(datos);
            }
        } 

        public int borrarInicio(){
            int elemento = inicio.dato;
            if (inicio == fin){
                inicio=fin=null;
            }else{
                inicio = inicio.siguiente;
                inicio.anterior= null;
            }
            return elemento;
        }

        public int borrarFinal(){
            int elemento = fin.dato;
            if (inicio == fin){
                inicio=fin=null;
            }else{
                fin = fin.anterior;
                fin.siguiente= null;
            }
            return elemento;
        }
                //metodo para eliminar un nodo especifico
                public void EliminarNodoEspecifico(int elemento){
            if(!estaVacia()){
                if(inicio==fin && elemento==inicio.dato){
                    inicio=fin=null;
                }else if(elemento==inicio.dato){
                    inicio=inicio.siguiente;
                }else{
                    PI anterior,temporal;
                    anterior=inicio;
                    temporal=inicio.siguiente;
                    while(temporal!=null && temporal.dato!=elemento){
                    anterior=anterior.siguiente;
                    temporal=temporal.siguiente;
                }
                    if(temporal!=null){
                       anterior.siguiente=temporal.siguiente;
                       if(temporal==fin){
                           fin=anterior;
                    }
                }
            }
        }
    }

    
    public static void main(String[] args) {
        long inicio = System.nanoTime();

                UAC Lista = new UAC();
                fila(Lista);
                Lista.ListaFinaInicio();
                Lista.borrarInicio();
                Lista.ListaFinaInicio();
                /*necesita el nodo donde esta el inquieto
                Lista.EliminarNodoEspecifico("aqui");*/
                System.out.println("El ultimo alumno  de la fila se fue ");
                    long fin = System.nanoTime();
                    double ta =(double)(fin-inicio)*30000.0e-9;
                    System.out.println("El tiempo que se tardo en atender toda la fila fueron:"+ ta+ " segundos");
    
}

        public static void fila(UAC NodoDoble){
            int na=NA.gna(10, 50);
                System.out.println("En la fila hay un total de "+na+" alumnos");

                tiempo p;
                int segundos;
                for (int i = 10; i < na; i++) {
                    
                    segundos=NA.gna(10, 15);
                    p = new tiempo(segundos);
                    System.out.println("Se ha integrado un nuevo alumno a la fila en:"+p.getsegundos()+" segundos");
                    NodoDoble.agregarInicio(i);
                }
                //intranquilo
                for (int in = na-2; in < na; in++) {
                    
                    segundos=NA.gna(10, 15);
                    p = new tiempo(segundos);
                    System.out.println("Se ha integrado un nuevo alumno intranquilo a la fila en:"+p.getsegundos()+" segundos");
                    NodoDoble.agregarInicio(in);
                }
            }
}

    
